package com.grabski.mike.mechanics.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.grabski.mike.mechanics.util.Constants;

/**
 * Created by mike on 12/20/17.
 */

public class GraphView extends View {

    private static float MAX_Y = 120f;
    private static float MAX_X = 5f;

    private float d0;
    private float L;

    private float propYMin = -1;
    private float propYMax = -1;
    private boolean isLegal = false;

    private Path functionPath;

    private Paint boundPaint;
    private Paint borderPaint;
    private Paint functionPaint;
    private Paint textPaint;


    public GraphView(Context context) {
        super(context);
        init();
    }

    public GraphView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GraphView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        boundPaint = new Paint();
        boundPaint.setColor(Color.argb(180, 255, 0, 0));
        boundPaint.setStyle(Paint.Style.FILL);

        borderPaint = new Paint();
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setStrokeWidth(4);

        functionPaint = new Paint();
        functionPaint.setStrokeWidth(10);
        functionPaint.setColor(Color.rgb(0, 0, 0));
        functionPaint.setStyle(Paint.Style.STROKE);

        textPaint = new Paint();
        textPaint.setStrokeWidth(4);
        textPaint.setColor(Color.rgb(0, 0, 0));
        textPaint.setTextSize(50);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (functionPath != null) {
            canvas.drawPath(functionPath, functionPaint);
        }
        if (propYMax > 0 || propYMin > 0) {
            if (!isLegal) {
                canvas.drawRect(0, propYMax, canvas.getWidth(), propYMin, boundPaint);
                canvas.drawText("" + (d0 + L) + "m", 10, propYMax, textPaint);
                canvas.drawText("" + d0 + "m", 10, propYMin, textPaint);
            } else {
                canvas.drawRect(0, propYMin, canvas.getWidth(), getHeight(), boundPaint);
                canvas.drawRect(0, propYMax, canvas.getWidth(), 0, boundPaint);
                canvas.drawText("" + (d0 +L) + "m", 10, propYMax, textPaint);
                canvas.drawText("" + d0 + "m", 10, propYMin, textPaint);
            }
        }
        canvas.drawLine(0, getMeasuredHeight() - 2, getMeasuredWidth(), getMeasuredHeight() - 2, borderPaint);
        canvas.drawLine(2, getMeasuredHeight() - 2, 2, 0, borderPaint);
        super.onDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    public void setBounds(float d0, float L) {
        this.d0 = d0;
        this.L = L;
        propYMin = getMeasuredHeight() - (d0 / MAX_Y) * getMeasuredHeight();
        propYMax = getMeasuredHeight() - ((d0 + L) / MAX_Y) * getMeasuredHeight();
        postInvalidateOnAnimation();
    }

    public void drawFunction(float v0, float A, float t) {
        functionPath = new Path();
        functionPath.moveTo(0, getMeasuredHeight());
        for (float x = 0; x < MAX_X && x < t; x += 0.01f) {
            float absx = x / MAX_X;
            float absY = (v0 * (x) - (A * (x) * (x) / 2f)) / MAX_Y;
            absx = getMeasuredWidth() * absx;
            absY = getMeasuredHeight() - absY * getMeasuredHeight();
            functionPath.lineTo(absx, absY);
        }
        postInvalidateOnAnimation();
    }

    public void setLegalBounds(float d0, float L) {
        this.d0 = d0;
        this.L = L;
        propYMin = getMeasuredHeight() - (d0 / MAX_Y) * getMeasuredHeight();
        propYMax = getMeasuredHeight() - ((L + d0) / MAX_Y) * getMeasuredHeight();
        isLegal = true;
        boundPaint.setColor(Color.argb(180, 0, 255, 0));
    }

    public void drawLegalFunction(float v0, float A, float t) {
        functionPath = new Path();
        functionPath.moveTo(0, getMeasuredHeight());
        for (float i = 0; i < Constants.MAX_SPEED && i < t; i += 0.01f) {
            float absx = 3.6f * (v0 - A * i) / Constants.MAX_SPEED;
            float absY = (v0 * i - A * i * i / 2f) / MAX_Y;
            absx = getMeasuredWidth() * absx;
            absY = getMeasuredHeight() - absY * getMeasuredHeight();
            functionPath.lineTo(absx, absY);
        }
        postInvalidateOnAnimation();
    }
}
