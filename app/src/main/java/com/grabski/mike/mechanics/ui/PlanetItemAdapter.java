package com.grabski.mike.mechanics.ui;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.grabski.mike.mechanics.R;
import com.grabski.mike.mechanics.project2.model.Planet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mike on 12/22/17.
 */

public class PlanetItemAdapter extends RecyclerView.Adapter<PlanetItemAdapter.ViewHolder> {

    private List<Planet> defaultItems;

    private List<Planet> items;
    private Context context;

    public PlanetItemAdapter(Context context, boolean includeInitialValues) {
        defaultItems = new ArrayList<>();
        items = new ArrayList<>();
        if (includeInitialValues) {
            defaultItems.add(new Planet(2, -2, 0, 0, -1, Color.RED));
            defaultItems.add(new Planet(1, -1, 0, 0, -2, Color.GREEN));

            defaultItems.add(new Planet(1, 1, 0, 0, 2, Color.BLUE));
            defaultItems.add(new Planet(2, 2, 0, 0, 1, Color.BLACK));
            items.addAll(defaultItems);
        }
        this.context = context;
    }

    @Override
    public PlanetItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_planet, parent, false));
    }

    @Override
    public void onBindViewHolder(PlanetItemAdapter.ViewHolder holder, int position) {
        holder.mass.setText("" + items.get(position).getMass());
        holder.X.setText("" + items.get(position).getX());
        holder.Y.setText("" + items.get(position).getY());
        holder.Vx.setText("" + items.get(position).getvX());
        holder.Vy.setText("" + items.get(position).getvY());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addPlanet(Planet planet) {
        items.add(planet);
        notifyItemInserted(items.size() - 1);
    }

    public ArrayList<Planet> getItems() {
        return new ArrayList<>(items);
    }

    public void clearItems() {
        items.clear();
        items.addAll(defaultItems);
        notifyDataSetChanged();
    }

    public void clearLatest() {
        if (items.isEmpty())
            return;
        items.remove(items.size() - 1);
        notifyItemRemoved(items.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mass;
        TextView X;
        TextView Y;
        TextView Vx;
        TextView Vy;

        public ViewHolder(View itemView) {
            super(itemView);
            mass = itemView.findViewById(R.id.mass);
            X = itemView.findViewById(R.id.x_cord);
            Y = itemView.findViewById(R.id.y_cord);
            Vx = itemView.findViewById(R.id.x_speed);
            Vy = itemView.findViewById(R.id.y_speed);
        }
    }
}
