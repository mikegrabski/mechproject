package com.grabski.mike.mechanics.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.grabski.mike.mechanics.project2.model.Planet;

import java.util.List;

/**
 * Created by mike on 12/22/17.
 */

public class PlanetDrawerView extends View {

    private boolean shouldSimulate = false;

    private long startTime;
    private long prevTime;

    private List<Planet> planets;

    public PlanetDrawerView(Context context) {
        super(context);
    }

    public PlanetDrawerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public PlanetDrawerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (shouldSimulate) {
            for (int i = 0; i < planets.size(); i++) {
                Planet planet = planets.get(i);
                long tempTime = System.currentTimeMillis() - prevTime;
                planet.draw(canvas, planets, tempTime);
            }
//            PointF camerapos = MechanicsUtils.calcCameraPosition(planets);
//            canvas.translate(camerapos.x * canvas.getWidth(), camerapos.y * canvas.getHeight());
            prevTime = System.currentTimeMillis();
            postInvalidateOnAnimation();
        }
    }

    public void setPlanets(List<Planet> planets) {
        this.planets = planets;
    }

    public void play() {
        shouldSimulate = true;
        startTime = System.currentTimeMillis();
        prevTime = startTime;
        postInvalidateOnAnimation();
    }

    public void pause() {
        shouldSimulate = false;
    }
}
