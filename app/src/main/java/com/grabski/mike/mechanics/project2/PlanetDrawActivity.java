package com.grabski.mike.mechanics.project2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.grabski.mike.mechanics.R;
import com.grabski.mike.mechanics.project2.model.Planet;
import com.grabski.mike.mechanics.ui.PlanetDrawerView;
import com.grabski.mike.mechanics.util.Constants;

import java.util.ArrayList;

/**
 * Created by mike on 12/22/17.
 */

public class PlanetDrawActivity extends AppCompatActivity {

    private PlanetDrawerView planetDrawerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project2);
        planetDrawerView = findViewById(R.id.planetDrawerView);
        ArrayList<Planet> planets = getIntent().getParcelableArrayListExtra(Constants.EXTRA_PLANET_LIST);
        planetDrawerView.setPlanets(planets);
    }


    public void play(View view) {
        planetDrawerView.play();
    }
}
