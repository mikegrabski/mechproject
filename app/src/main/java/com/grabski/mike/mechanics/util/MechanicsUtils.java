package com.grabski.mike.mechanics.util;

import android.graphics.Color;
import android.graphics.PointF;
import android.widget.TextView;

import com.grabski.mike.mechanics.project2.model.Planet;

import java.util.List;

/**
 * Created by mike on 12/2/17.
 */

public class MechanicsUtils {

    public static CarState shouldGaz(float v0, float d0, float L, float Ty, float Aa, float Ad) {
        if (v0 * Ty - (Ad * Ty * Ty) / 2f <= d0 && v0 / Ad < Ty) {
            return CarState.TORMOZ;
        }
        if (v0 * Ty + (Aa * Ty * Ty) / 2f >= d0 + L) {
            return CarState.GAZ;
        }
        return CarState.SHTRAF;
    }

    public static float cosine(float x1, float y1, float x2, float y2) {
        return (float) ((x2 - x1) / Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)));
    }

    public static float sine(float x1, float y1, float x2, float y2) {
        return (float) ((y2 - y1) / Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)));
    }

    public static PointF acceleration(Planet planetA, Planet planetB) {
        float aX;
        float aY;
        float divisor = (float) Math.sqrt(Math.pow(planetB.getX() - planetA.getX(), 2) + Math.pow(planetB.getY() - planetA.getY(), 2));

        aX = (planetB.getMass() * cosine(planetA.getX(), planetA.getY(), planetB.getX(), planetB.getY())) /
                divisor;
        aY = (planetB.getMass() * sine(planetA.getX(), planetA.getY(), planetB.getX(), planetB.getY())) /
                divisor;

        return new PointF(aX, aY);
    }

    public static PointF calcCameraPosition(List<Planet> planetList){
        float xSum = 0;
        float ySum = 0;
        for (Planet planet : planetList){
            xSum += planet.getX();
            ySum += planet.getY();
        }
        xSum = xSum/planetList.size();
        ySum = ySum/planetList.size();
        float propX = (xSum + 10) / 20f;
        float propY = 1 - (ySum + 10) / 20f;
        return new PointF(propX,propY);
    }

    public static float distance(Planet planetA, Planet planetB) {
        return (float) Math.sqrt(Math.pow(planetB.getX() - planetA.getX(), 2) + Math.pow(planetB.getY() - planetA.getY(), 2));
    }

    public enum CarState {
        GAZ,
        TORMOZ,
        SHTRAF;

        public void applyToTextview(TextView textView) {
            switch (this) {
                case GAZ:
                    textView.setTextColor(Color.GREEN);
                    textView.setText("Accelerate");
                    break;
                case TORMOZ:
                    textView.setTextColor(Color.RED);
                    textView.setText("Deccelerate");
                    break;
                case SHTRAF:
                    textView.setTextColor(Color.BLACK);
                    textView.setText("\uD83D\uDE93 \uD83D\uDCB0 \uD83D\uDCB0 \uD83D\uDE22");
                    break;
            }
        }
    }
}
