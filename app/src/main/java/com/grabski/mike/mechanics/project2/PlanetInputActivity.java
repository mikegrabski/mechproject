package com.grabski.mike.mechanics.project2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.grabski.mike.mechanics.R;
import com.grabski.mike.mechanics.project2.model.Planet;
import com.grabski.mike.mechanics.ui.PlanetItemAdapter;
import com.grabski.mike.mechanics.util.Constants;

/**
 * Created by mike on 12/2/17.
 */

public class PlanetInputActivity extends AppCompatActivity {

    private PlanetItemAdapter planetItemAdapter;

    private EditText massText;
    private EditText xCoordText;
    private EditText yCoordText;
    private EditText vXText;
    private EditText vYText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivty_project2input);
        RecyclerView recyclerView = findViewById(R.id.input_recycler);
        planetItemAdapter = new PlanetItemAdapter(this, true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(planetItemAdapter);
        massText = findViewById(R.id.mass);
        xCoordText = findViewById(R.id.x_cord);
        yCoordText = findViewById(R.id.y_cord);
        vXText = findViewById(R.id.x_speed);
        vYText = findViewById(R.id.y_speed);
    }

    public void addPlanet(View view) {
        if (massText.getText().toString().equals("") ||
                xCoordText.getText().toString().equals("") ||
                yCoordText.getText().toString().equals("") ||
                vXText.getText().toString().equals("") ||
                vYText.getText().toString().equals("")) {
            Toast.makeText(this, "Please Fill in all the boxes..", Toast.LENGTH_LONG).show();
            return;
        }
        float massText = Float.parseFloat(this.massText.getText().toString());
        float xCoord = Float.parseFloat(this.xCoordText.getText().toString());
        float yCoord = Float.parseFloat(this.yCoordText.getText().toString());
        float vX = Float.parseFloat(this.vXText.getText().toString());
        float vY = Float.parseFloat(this.vYText.getText().toString());

        planetItemAdapter.addPlanet(new Planet(massText, xCoord, yCoord, vX, vY));
    }

    public void simulate(View view) {
        Intent simulateIntent = new Intent(PlanetInputActivity.this, PlanetDrawActivity.class);
        simulateIntent.putParcelableArrayListExtra(Constants.EXTRA_PLANET_LIST,  planetItemAdapter.getItems());
        startActivity(simulateIntent);
    }

    public void clear(View view) {
        planetItemAdapter.clearItems();
    }

    public void deleteLatest(View view) {
        planetItemAdapter.clearLatest();
    }
}
