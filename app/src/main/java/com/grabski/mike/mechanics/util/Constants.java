package com.grabski.mike.mechanics.util;

/**
 * Created by mike on 12/20/17.
 */

public class Constants {

    //Project 1
    public static final float MAX_SPEED = 123.2f;


    //Project 2
    public static final int MASS_RADIUS_MULTIPLE = 11;
    public static final float GRAPH_BOUND = 10f;

    public static final String EXTRA_PLANET_LIST = "PLANETS";
}
