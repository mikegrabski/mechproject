package com.grabski.mike.mechanics.project2.model;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;

import com.grabski.mike.mechanics.util.Constants;
import com.grabski.mike.mechanics.util.MechanicsUtils;

import java.util.List;
import java.util.Random;

/**
 * Created by mike on 12/22/17.
 * <p>
 * A model for Planets, includes planets mass, current position and velocity
 * Implementing parcelable is necessary in order to be able to pass planet's list from one Activity to another
 */

public class Planet implements Parcelable {

    private int color = -1;
    private float mass;
    private float x;
    private float y;
    private float vX;
    private float vY;


    private Paint paint;

    public Planet(float mass, float x, float y, float vX, float vY, int color) {
        this.mass = mass;
        this.x = x;
        this.y = y;
        this.vX = vX;
        this.vY = vY;
        this.color = color;
    }

    public Planet(float mass, float x, float y, float vX, float vY) {
        this.mass = mass;
        this.x = x;
        this.y = y;
        this.vX = vX;
        this.vY = vY;
        this.color = color;
    }

    public float getMass() {
        return mass;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getvX() {
        return vX;
    }

    public float getvY() {
        return vY;
    }

    /**
     * Handles calculation and drawing of the planets on the canvas.
     *
     * @param canvas  Canvas to draw the resultant planet positions
     * @param planets List of planets in the universe
     * @param deltaT  Time offset from previous drawCall in milliseconds
     */
    public void draw(Canvas canvas, List<Planet> planets, long deltaT) {
        if (paint == null) {
            paint = new Paint();
            paint.setColor(color == -1 ? new Random().nextInt() : color);
            paint.setStyle(Paint.Style.FILL);
        }
        //In some cases duplicate frames are requested and deltaT is 0, in that case just skip
        if (deltaT == 0)
            return;

        PointF finalAcceleration = new PointF(0, 0);
        //Going through the list of planets in the universe and adding the acceleration that impacts this planet.
        for (int i = 0; i < planets.size(); i++) {
            Planet otherPlanet = planets.get(i);
            if (!otherPlanet.equals(this)) {
                //Since we take radius 11*mass, if distance is less then radius, merge planets
                if (MechanicsUtils.distance(this, otherPlanet) < mass / Constants.MASS_RADIUS_MULTIPLE) {
                    mass = mass + otherPlanet.mass;
                    planets.remove(i);
                    continue;
                }
                PointF acceleration = MechanicsUtils.acceleration(this, otherPlanet);
                finalAcceleration.x += acceleration.x;
                finalAcceleration.y += acceleration.y;
            }
        }
        //Since we used seconds for acceleration calculation divide by 1000 to convert to seconds from ms
        float delta = (deltaT / 1000f);
        vX += (finalAcceleration.x * delta);
        vY += (finalAcceleration.y * delta);
        x += (vX * delta + finalAcceleration.x * delta * delta / 2f);
        y += (vY * delta + finalAcceleration.y * delta * delta / 2f);

        float propX = (x + Constants.GRAPH_BOUND / (2 * Constants.GRAPH_BOUND));
        float propY = (y + Constants.GRAPH_BOUND) / (2 * Constants.GRAPH_BOUND);
        propY = 1 - propY; //In android y=0 is in the top, so reverse
        canvas.drawCircle(propX * canvas.getWidth(), propY * canvas.getHeight(), mass * Constants.MASS_RADIUS_MULTIPLE, paint);
    }

    //PARCELABLE necessary functions

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(color);
        dest.writeFloat(mass);
        dest.writeFloat(x);
        dest.writeFloat(y);
        dest.writeFloat(vX);
        dest.writeFloat(vY);
    }


    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Planet> CREATOR = new Parcelable.Creator<Planet>() {
        @Override
        public Planet createFromParcel(Parcel in) {
            return new Planet(in);
        }

        @Override
        public Planet[] newArray(int size) {
            return new Planet[size];
        }
    };


    protected Planet(Parcel in) {
        color = in.readInt();
        mass = in.readFloat();
        x = in.readFloat();
        y = in.readFloat();
        vX = in.readFloat();
        vY = in.readFloat();
    }
}