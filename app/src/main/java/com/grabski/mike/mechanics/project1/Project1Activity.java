package com.grabski.mike.mechanics.project1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.grabski.mike.mechanics.R;
import com.grabski.mike.mechanics.ui.GraphView;
import com.grabski.mike.mechanics.util.MechanicsUtils;

/**
 * Created by mike on 12/2/17.
 */

public class Project1Activity extends AppCompatActivity {

    private EditText v0_text;
    private EditText d0_text;
    private EditText L_text;
    private EditText Ty_text;
    private EditText Aa_text;
    private EditText Ad_text;

    private TextView resultText;
    private GraphView graphView;
    private GraphView graphView2;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivty_project1);
        initViews();
    }

    private void initViews() {
        v0_text = findViewById(R.id.v0_text);
        d0_text = findViewById(R.id.d0_text);
        L_text = findViewById(R.id.L_text);
        Ty_text = findViewById(R.id.Ty_text);
        Aa_text = findViewById(R.id.Aa_text);
        Ad_text = findViewById(R.id.Ad_text);
//        InputFilterMinMax.setInputFilterOn(v0_text, 20, 80);
//        InputFilterMinMax.setInputFilterOn(d0_text, 7, 50);
//        InputFilterMinMax.setInputFilterOn(L_text, 7, 50);
//        InputFilterMinMax.setInputFilterOn(Ty_text, 2, 4);
//        InputFilterMinMax.setInputFilterOn(Aa_text, 1, 3);
//        InputFilterMinMax.setInputFilterOn(Ad_text, 1, 3);
        graphView = findViewById(R.id.graphView);
        graphView2 = findViewById(R.id.graphView2);
        resultText = findViewById(R.id.result);
    }

    public void doItClicked(View view) {
        if (v0_text.getText().toString().equals("") ||
                d0_text.getText().toString().equals("") ||
                L_text.getText().toString().equals("") ||
                Ty_text.getText().toString().equals("") ||
                Aa_text.getText().toString().equals("") ||
                Ad_text.getText().toString().equals("")) {
            Toast.makeText(this, "Please Fill in all the boxes..", Toast.LENGTH_LONG).show();
            return;
        }
        float v0 = Float.parseFloat(v0_text.getText().toString()) / 3.6f;
        float d0 = Float.parseFloat(d0_text.getText().toString());
        float L = Float.parseFloat(L_text.getText().toString());
        float Ty = Float.parseFloat(Ty_text.getText().toString());
        float Aa = Float.parseFloat(Aa_text.getText().toString());
        float Ad = Float.parseFloat(Ad_text.getText().toString());


        graphView.setBounds(d0, L);
        graphView2.setLegalBounds(d0, L);
        MechanicsUtils.CarState state = MechanicsUtils.shouldGaz(v0, d0, L, Ty, Aa, Ad);

        if (state == MechanicsUtils.CarState.TORMOZ) {
            graphView.drawFunction(v0, Ad, v0 / Ad);
            graphView2.drawLegalFunction(v0, Ad, v0 / Ad);
        } else {
            graphView.drawFunction(v0, -Aa, Ty);
            graphView2.drawLegalFunction(v0, -Aa, Ty);
        }

        state.applyToTextview(resultText);
    }
}
